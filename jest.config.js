const config = {
  verbose: true,
  "collectCoverageFrom": [    
    "src/components/**/*.tsx",

    "src/context/**/*.js",
    "src/context/**/*.jsx",
    "src/context/**/*.tsx",
    
    "!src/**/index.tsx",    
    "!src/**/_app.tsx",
    "!src/**/hello.ts"
  ],
  "coverageThreshold": {
    "global": {
      "branches": 70,
      "functions": 70,
      "lines": 70,
      "statements": 70
    }
  },
  "collectCoverage": true,
  "moduleNameMapper": {
    "\\.(css|less|scss)$": "identity-obj-proxy"
  }
  
};

module.exports = config;