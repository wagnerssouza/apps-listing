export interface WrapperProps {
  children: JSX.Element | JSX.Element[];
}

export interface Menu {
  value: String;
}

export interface AppDescription {
  id: String;
  name: String;
  description: String;
  categories: String[];
  subscriptions: subscriptionItem[];
}

export interface subscriptionItem {
  name: String;
  price: any;
}

export interface AppContextType {
  menuCategories: Menu[];
  listApps: AppDescription[];
  updateListApps: (data: AppDescription[]) => void;
  listAppsFiltered: AppDescription[];
  updateListAppsFilteredByCategory: (data: String) => void;
  updateListAppsFilteredByInput: (searchText: String, event: String) => void;
}

export type PaginationType = {
  items: AppDescription[];
  itemsPerPage: number;
}