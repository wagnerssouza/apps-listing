
import List from '../components/List'
import Nav from '../components/Nav'

export default function Home() {
  return (    
    <div className="flex-container">
      <Nav />
      <List />
    </div>
  )
}
