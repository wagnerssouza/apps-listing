import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { ListAppProvider } from '../context/ListAppContext';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ListAppProvider>
      <Component {...pageProps} />
    </ListAppProvider>
  )
}
export default MyApp
