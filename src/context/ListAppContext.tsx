import { useState, useCallback } from "react";
import { createContext } from "use-context-selector";
import {
  AppContextType,
  AppDescription,
  Menu,
  WrapperProps,
} from "../interfaces/interfaces";

export const formatCurrency = (listApps: AppDescription[]) => {
  return listApps.map((item) => {
    item.subscriptions.forEach((i) => {
      const x = Array.from(i.price.toString());
      const positionDot = x.length > 4 ? 2 : 1;
      let arr = x;
      const x1 = arr
        .map((item, i) => {
          return (arr[i] = i == positionDot ? `${item}.` : `${item}`);
        })
        .join("");
      i.price = x1;
    });
    return item;
  });
};

export const sortListByPriceAscending = (listApps: AppDescription[]) => {
  return listApps
    .map((app) => {
      const totalPrice = app.subscriptions.reduce((total, current) => {
        return total + current.price;
      }, 0);
      return { ...app, totalPrice };
    })
    .sort((a, b) => {
      return a.totalPrice - b.totalPrice;
    });
};

export const filteredByText = (
  listApps: AppDescription[],
  searchText: String
) => {
  return listApps.filter((item) => {
    return item.name.toLowerCase().search(searchText.toLowerCase()) !== -1;
  });
};

export const filteredByCategory = (
  listApps: AppDescription[],
  category: String
) => {
  return category === "All"
    ? listApps
    : listApps.filter((item) => item.categories.some((c) => c === category));
};

export const createMenuCategories = (data: AppDescription[]) => {
  const arrItemsMenu = data.flatMap((item) => item.categories).sort();
  return [...new Set(arrItemsMenu)].map((value: any) => {
    return { value };
  });
};

export const ListAppContext = createContext({} as AppContextType);

export function ListAppProvider({ children }: WrapperProps) {
  const [menuCategories, setMenuCategories] = useState<Menu[]>([]);
  const [listApps, setListApps] = useState<AppDescription[]>([]);
  const [actualCategoryFiltered, setActualCategoryFiltered] =
    useState<String>("");
  const [listAppsFiltered, setListAppsFiltered] =
    useState<AppDescription[]>(listApps);

  const updateListApps = useCallback((data: AppDescription[]) => {
    const ascendingList = sortListByPriceAscending(data);
    const listAppsCurrencyFormatted = formatCurrency(ascendingList);
    setListApps(() => listAppsCurrencyFormatted);
    setListAppsFiltered(() => listAppsCurrencyFormatted);
    setMenuCategories(() => [...createMenuCategories(data)]);
  }, []);

  const updateListAppsFilteredByCategory = useCallback(
    (category: String) => {
      setListAppsFiltered(() => [...filteredByCategory(listApps, category)]);
      setActualCategoryFiltered(category);
    },
    [listApps]
  );

  const updateListAppsFilteredByInput = useCallback(
    (searchText: String, event: String) => {
      var listByCategory = filteredByCategory(
        listApps,
        actualCategoryFiltered ? actualCategoryFiltered : "All"
      );
      setListAppsFiltered(() =>
        event === "change"
          ? [...filteredByText(listByCategory, searchText)]
          : listByCategory
      );
    },
    [actualCategoryFiltered, listApps]
  );

  return (
    <ListAppContext.Provider
      value={{
        menuCategories,
        listApps,
        updateListApps,
        listAppsFiltered,
        updateListAppsFilteredByCategory,
        updateListAppsFilteredByInput,
      }}
    >
      {children}
    </ListAppContext.Provider>
  );
}
