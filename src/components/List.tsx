import { useEffect } from "react";
import { useListApps } from "../hooks/useListApps";
import { useListAppsFilteredByCategory } from "../hooks/useListAppsFilteredByCategory";
import Pagination from "./Pagination";
import Search from "./Search";

const List = () => {
  const { updateListApps } = useListApps();
  const { listAppsFiltered } = useListAppsFilteredByCategory();
  const itemsPerPage = 3;
  const url = "https://run.mocky.io/v3/73aeddf6-2796-42c6-8796-8733fcf4c6b8";

  useEffect(() => {
    fetch(url)
      .then((response) => response.json())
      .then((json) => {
        updateListApps(json)
      }).catch((error) => {
        updateListApps([])
      })
  }, [updateListApps, url]);
 
  return (
      <section className="apps-list" aria-label="apps-list">
        <Search />
        <Pagination items={listAppsFiltered} itemsPerPage={itemsPerPage} />        
      </section>
  );
}

export default List;
