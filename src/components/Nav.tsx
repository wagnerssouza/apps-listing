import { useState } from "react";
import { useListAppsFilteredByCategory } from "../hooks/useListAppsFilteredByCategory";
import { useMenu } from "../hooks/useMenu";
import styles from "../styles/Nav.module.css";

const Nav = () => {
  const { menuCategories } = useMenu();
  const { updateListAppsFilteredByCategory } = useListAppsFilteredByCategory();
  const [activeItem, setActiveItem] = useState("All");

  const clickCategory = (event: any) => {
    const el = event.target as HTMLElement;    
    updateListAppsFilteredByCategory(el.innerText);
    setActiveItem(el.id);
  };

  return (
    <nav className={styles["nav-categories"]}>
      <h2>Categories</h2>

      <ul className={styles["nav-menu"]} aria-label="nav-menu">
        <li key="123456">
          <a
            href="#"
            className={activeItem === "All" ? `${styles.active}` : ""}
            onClick={clickCategory}
            id="All"
          >
            All
          </a>
        </li>
        {menuCategories?.map((i, index) => (
          <li key={index}>
            <a
              href="#"
              className={activeItem === i.value ? `${styles.active}` : ""}
              id={i.value.toString()}
              onClick={clickCategory}
            >
              {i.value}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Nav;
