import { useEffect, useState } from "react";
import { AppDescription, PaginationType } from "../interfaces/interfaces";
import styles from "../styles/Pagination.module.css";

const Pagination = ({ items, itemsPerPage }: PaginationType) => {
  const [currentPage, setCurrentPage] = useState(1);
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = items?.slice(indexOfFirstItem, indexOfLastItem);
  const pages: Number[] = [];

  useEffect(() => {
    setCurrentPage(1);
  }, [items]);

  const handleClick = (event: any) => {
    setCurrentPage(Number((event.target as HTMLFormElement).id));
  };

  const handleNextBtn = () => {
    setCurrentPage((currentPage) => currentPage + 1);
  };

  const handlePrevBtn = () => {
    setCurrentPage((currentPage) => currentPage - 1);
  };

  const renderData = (data: AppDescription[]) => {
    return (
      <ul aria-label="list-items">
        {data && data?.length > 0 ? (
          data?.map((todo, index) => {
            return (
              <li key={index.toString()}>
                <div className={styles["app-item"]}>
                  <div className={styles["box-info"]}>
                    <div className={styles["box-info--content"]}>
                      <div className={styles.description}>
                        <h1>{todo.name}</h1>
                        <p>{todo.description}</p>
                      </div>
                      <div className={styles.tags}>
                        {todo?.categories?.map((category, index) => {
                          return (
                            <span key={index}>
                              {category}
                              {index !== todo.categories.length - 1
                                ? " / "
                                : ""}
                            </span>
                          );
                        })}
                      </div>
                    </div>
                    <div className={styles["box-info--footer"]}>
                      <ul>
                        {todo?.subscriptions?.map((subscription, index) => {
                          return (
                            <li key={index.toString()}>
                              <span>{subscription.name}</span>
                              <h3>
                                {subscription.price > 0 ? (
                                  <>
                                    {subscription.price} <sup>€</sup>
                                  </>
                                ) : (
                                  <>
                                    {"Free"} <sup></sup>
                                  </>
                                )}
                              </h3>
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
            );
          })
        ) : (
          <div>
            <h1 aria-label="no-results">No results ...</h1>
          </div>
        )}
      </ul>
    );
  };

  for (let i = 1; i <= Math.ceil(items?.length / itemsPerPage); i++) {
    pages.push(i);
  }

  const renderPageNumbers = pages?.map((number, index) => {
    return (
      <li
        key={number.toString()}
        id={number.toString()}
        aria-label={`current-page-${index}`}
        onClick={handleClick}
        className={currentPage == number ? `${styles.active}` : ""}
      >
        {number}
      </li>
    );
  });

  return (
    <>
      {renderData(currentItems)}
      {pages?.length > 0 ? (
        <div className={styles.pagination}>
          <ul>
            <li
              aria-label="btn-prev"
              onClick={handlePrevBtn}
              className={currentPage == pages[0] ? `${styles.disabled}` : ""}
            >
              &lt;
            </li>
            {renderPageNumbers}
            <li
              aria-label="btn-next"
              onClick={handleNextBtn}
              className={
                currentPage == pages[pages.length - 1]
                  ? `${styles.disabled}`
                  : ""
              }
            >
              &gt;
            </li>
          </ul>
        </div>
      ) : (
        ``
      )}
    </>
  );
}

export default Pagination;
