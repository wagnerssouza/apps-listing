import { useListAppsFilteredByInput } from "../hooks/useListAppsFilteredInput";

const Search = () => {
  const { updateListAppsFilteredByInput } = useListAppsFilteredByInput();

  const searchList = (event: any) => {
    const el = (event.target as HTMLFormElement).value;

    if(el.length === 0) {
      updateListAppsFilteredByInput('', 'blur');
    } else {
      updateListAppsFilteredByInput(el, 'change');
    }
    
  };

  const blurSearchInput = (event: any) => {
    let el = (event.target as HTMLFormElement);
    updateListAppsFilteredByInput(el.value = '', 'blur');
  }

  return (
    <>
      <header>
        <input type="text" aria-label="search-input" onBlur={blurSearchInput} onChange={searchList} placeholder="Search by App" />
      </header>
    </>
  );
}

export default Search;