import { useContextSelector } from "use-context-selector";
import { ListAppContext } from "../context/ListAppContext";

export function useListAppsFilteredByCategory() {
  const listAppsFiltered = useContextSelector(ListAppContext, listApp => listApp.listAppsFiltered);
  const updateListAppsFilteredByCategory = useContextSelector(ListAppContext, listApp => listApp.updateListAppsFilteredByCategory);
  return {
    listAppsFiltered,
    updateListAppsFilteredByCategory
  };
}
