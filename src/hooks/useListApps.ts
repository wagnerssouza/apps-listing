import { useContextSelector } from "use-context-selector";
import { ListAppContext } from "../context/ListAppContext";

export function useListApps() {
  const listApps = useContextSelector(ListAppContext, listApp => listApp.listApps);
  const updateListApps = useContextSelector(ListAppContext, listApp => listApp.updateListApps);
  return {
    listApps,
    updateListApps
  };
}
