import { useContextSelector } from "use-context-selector";
import { ListAppContext } from "../context/ListAppContext";

export function useMenu() {
  const menuCategories = useContextSelector(ListAppContext, listApp => listApp.menuCategories);
  return {
    menuCategories
  };
}