import { useContextSelector } from "use-context-selector";
import { ListAppContext } from "../context/ListAppContext";

export function useListAppsFilteredByInput() {
  const updateListAppsFilteredByInput = useContextSelector(ListAppContext, listApp => listApp.updateListAppsFilteredByInput);
  return {
    updateListAppsFilteredByInput    
  };
}
