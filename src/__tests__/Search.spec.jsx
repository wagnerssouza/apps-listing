import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Search from '../components/Search';
import { useListAppsFilteredByInput } from '../hooks/useListAppsFilteredInput';
import { useListAppsFilteredByCategory } from '../hooks/useListAppsFilteredByCategory';


const mockSpy = jest.fn();
jest.mock('../hooks/useListAppsFilteredInput');
jest.mock('../hooks/useListAppsFilteredByCategory');


describe("Component Search", () => {

  it('Should render Search component', () => {
    useListAppsFilteredByInput.mockImplementation(() => {
      return {
        updateListAppsFilteredByInput: mockSpy
      }
    });

    const { getByLabelText } = render(<Search />);
    const input = getByLabelText('search-input');
    
    expect(input).toBeInTheDocument();
  });

  it('Should change value in input search', () => {
    useListAppsFilteredByInput.mockImplementation(() => {
      return {
        updateListAppsFilteredByInput: mockSpy
      }
    });

    const { getByLabelText } = render(<Search />);
    const input = getByLabelText('search-input');
    fireEvent.change(input, {target: {value: 'smart'}})
    expect(input.value.length).toBe(5);
  });

  it('Should reset list items when field is empty', () => {
    useListAppsFilteredByInput.mockImplementation(() => {
      return {
        updateListAppsFilteredByInput: mockSpy
      }
    });

    useListAppsFilteredByCategory.mockImplementation(() => {
      return {
        updateListAppsFilteredByCategory: mockSpy
      }
    });

    const { getByLabelText } = render(<Search />);
    const input = getByLabelText('search-input');
    fireEvent.change(input, {target: {value: 'a'}})
    fireEvent.change(input, {target: {value: ''}})
    expect(input.value.length).toBe(0);

  });

  it('Should clear field on blur event', () => {
    useListAppsFilteredByInput.mockImplementation(() => {
      return {
        updateListAppsFilteredByInput: mockSpy
      }
    });
    const { getByLabelText } = render(<Search />);
    const input = getByLabelText('search-input');
    fireEvent.blur(input)
    expect(input.value).toBe('');
    expect(input.value.length).toBe(0);
  });

});