import React from "react";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { useListAppsFilteredByCategory } from "../hooks/useListAppsFilteredByCategory";
import { useListApps } from "../hooks/useListApps";
import List from "../components/List";
jest.mock("../hooks/useListAppsFilteredByCategory");
jest.mock("../hooks/useListApps");
const mockSpy = jest.fn();

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve([
        {
          id: "9b565b11-7311-5b5e-a699-97873dffb361",
          name: "Voice Report",
          description: "Calls reporting and analytics of your calls.",
          categories: ["Voice Analytics", "Reporting", "Optimization"],
          subscriptions: [
            {
              name: "Trial",
              price: 0,
            },
            {
              name: "Professional",
              price: 3500,
            },
          ],
        },
      ]),
  })
);

describe("Component List", () => {
  it("should render List component", async () => {
    useListApps.mockImplementation(() => {
      return {
        updateListApps: mockSpy,
      };
    });

    useListAppsFilteredByCategory.mockImplementation(() => {
      return {
        useListAppsFilteredByCategory: mockSpy,
      };
    });

    const { getByLabelText } = render(<List />);
    const input = getByLabelText("apps-list");

    expect(input).toBeInTheDocument();
  });

  it(`Should on reject fetch show visible feedback message`, () => {
    global.fetch = jest.fn(() => Promise.reject("error"));
    const { getByLabelText } = render(<List />);
    const input = getByLabelText("no-results");

    expect(input).toBeInTheDocument();
    expect(input.innerHTML).toContain("No results ...");
  });
});
