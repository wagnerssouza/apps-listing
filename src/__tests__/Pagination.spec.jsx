import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import Pagination from "../components/Pagination";

describe("Component Pagination", () => {

  it("Should render component Pagination", () => {
    const { getByLabelText } = render(
      <Pagination items={[{}]} itemsPerPage={3} />
    );
    const element = getByLabelText("list-items");

    expect(element).toBeInTheDocument();
  });

  it("Should render list items", () => {
    const { getByLabelText } = render(
      <Pagination
        items={[
          {
            id: "52714d80-e3c4-5593-b9a3-e2ff484be372",
            name: "Smart Text",
            description: "Use SMS to help you communicate with your customers.",
            categories: ["Channels", "Dialer"],
            subscriptions: [
              {
                name: "Trial",
                price: 0,
              },
              {
                name: "Professional",
                price: 3000,
              },
            ],
          },
        ]}
        itemsPerPage={3}
      />
    );
    const listItems = getByLabelText("list-items");
    expect(listItems).toMatchInlineSnapshot(`
      <ul
        aria-label="list-items"
      >
        <li>
          <div
            class="app-item"
          >
            <div
              class="box-info"
            >
              <div
                class="box-info--content"
              >
                <div
                  class="description"
                >
                  <h1>
                    Smart Text
                  </h1>
                  <p>
                    Use SMS to help you communicate with your customers.
                  </p>
                </div>
                <div
                  class="tags"
                >
                  <span>
                    Channels
                     / 
                  </span>
                  <span>
                    Dialer
                    
                  </span>
                </div>
              </div>
              <div
                class="box-info--footer"
              >
                <ul>
                  <li>
                    <span>
                      Trial
                    </span>
                    <h3>
                      Free
                       
                      <sup />
                    </h3>
                  </li>
                  <li>
                    <span>
                      Professional
                    </span>
                    <h3>
                      3000
                       
                      <sup>
                        €
                      </sup>
                    </h3>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </li>
      </ul>
    `);
  });

  it("Should check active class on page 1", () => {
    const { getByLabelText } = render(
      <Pagination items={[{}]} itemsPerPage={3} />
    );
    const firstLi = getByLabelText("current-page-0");

    expect(firstLi).toHaveClass("active");
  });

  it("Should check active class after click", () => {
    const { getByLabelText } = render(
      <Pagination items={[{}, {}, {}, {}]} itemsPerPage={3} />
    );
    const pageOne = getByLabelText("current-page-0");
    const pageTwo = getByLabelText("current-page-1");

    fireEvent.click(pageTwo);

    expect(pageOne).not.toHaveClass("active");
    expect(pageTwo).toHaveClass("active");
  });

  it("Should check active class after click prev button", () => {
    const { getByLabelText } = render(
      <Pagination items={[{}, {}, {}, {}]} itemsPerPage={3} />
    );
    const pageOne = getByLabelText("current-page-0");
    const pageTwo = getByLabelText("current-page-1");
    const btnPrev = getByLabelText("btn-prev");

    fireEvent.click(pageTwo);
    fireEvent.click(btnPrev);

    expect(pageOne).toHaveClass("active");
    expect(pageTwo).not.toHaveClass("active");
  });

  it("Should check active class after click next button", () => {
    const { getByLabelText } = render(
      <Pagination items={[{}, {}, {}, {}]} itemsPerPage={3} />
    );
    const pageOne = getByLabelText("current-page-0");
    const pageTwo = getByLabelText("current-page-1");
    const btnNext = getByLabelText("btn-next");

    fireEvent.click(btnNext);

    expect(pageOne).not.toHaveClass("active");
    expect(pageTwo).toHaveClass("active");
  });
  
});
