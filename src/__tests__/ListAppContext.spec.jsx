import '@testing-library/jest-dom/extend-expect';
import {filteredByText, sortListByPriceAscending, filteredByCategory, createMenuCategories, formatCurrency } from '../context/ListAppContext';

describe("Context", () => {
 
  it('Should check array of apps filtered by text', () => {
    const filtered = filteredByText([{
      "id": "52714d80-e3c4-5593-b9a3-e2ff484be372",
      "name": "Smart Text",
      "description":
        "Use SMS to help you communicate with your customers.",
      "categories": ["Channels"],
      "subscriptions": [
        {
          "name": "Trial",
          "price": 0
        }
      ]
    },
    {
      "id": "8d68c357-59e6-505a-b0e1-4953196b14df",
      "name": "Customer Chat",
      "description": "Improve your call center with live chat support.",
      "categories": ["Channels"],
      "subscriptions": [
        {
          "name": "Trial",
          "price": 0
        }
      ]
    }], "Smart Text");

    expect(filtered).toStrictEqual([{"categories": ["Channels"], "description": "Use SMS to help you communicate with your customers.", "id": "52714d80-e3c4-5593-b9a3-e2ff484be372", "name": "Smart Text", "subscriptions": [{"name": "Trial", "price": 0}]}]);
  });

  it('Should verify array sort by price ascending', () => {
    const listAscending = sortListByPriceAscending([{
      "id": "52714d80-e3c4-5593-b9a3-e2ff484be372",
      "name": "Smart Text",
      "description":
        "Use SMS to help you communicate with your customers.",
      "categories": ["Channels"],
      "subscriptions": [
        {
          "name": "Starter",
          "price": 2000
        },
        {
          "name": "Plus",
          "price": 4500
        }
      ]
    },
    {
      "id": "dd024ed5-efae-5785-addc-09e592066e5c",
      "name": "Report Plus",
      "description": "Advanced reporting with custom dashboards.",
      "categories": ["Reporting"],
      "subscriptions": [
        {
          "name": "Trial",
          "price": 0
        }
      ]
    }]);

    expect(listAscending).toStrictEqual([{"categories": ["Reporting"], "description": "Advanced reporting with custom dashboards.", "id": "dd024ed5-efae-5785-addc-09e592066e5c", "name": "Report Plus", "subscriptions": [{"name": "Trial", "price": 0}], "totalPrice": 0}, {"categories": ["Channels"], "description": "Use SMS to help you communicate with your customers.", "id": "52714d80-e3c4-5593-b9a3-e2ff484be372", "name": "Smart Text", "subscriptions": [{"name": "Starter", "price": 2000}, {"name": "Plus", "price": 4500}], "totalPrice": 6500}]);
  });

  it('Should verify array filtered by category', () => {
    const arrayFilteredByCategory = filteredByCategory([{
      "id": "52714d80-e3c4-5593-b9a3-e2ff484be372",
      "name": "Smart Text",
      "description":
        "Use SMS to help you communicate with your customers.",
      "categories": ["Channels"],
      "subscriptions": [
        {
          "name": "Starter",
          "price": 2000
        },
        {
          "name": "Plus",
          "price": 4500
        }
      ]
    },
    {
      "id": "dd024ed5-efae-5785-addc-09e592066e5c",
      "name": "Report Plus",
      "description": "Advanced reporting with custom dashboards.",
      "categories": ["Reporting"],
      "subscriptions": [
        {
          "name": "Trial",
          "price": 0
        }
      ]
    }], `Channels`);

    

    expect(arrayFilteredByCategory).toStrictEqual([{"categories": ["Channels"], "description": "Use SMS to help you communicate with your customers.", "id": "52714d80-e3c4-5593-b9a3-e2ff484be372", "name": "Smart Text", "subscriptions": [{"name": "Starter", "price": 2000}, {"name": "Plus", "price": 4500}]}]);
  });

  it('Should return menu of categories', () => {
    const menuCategories = createMenuCategories([{
      "id": "52714d80-e3c4-5593-b9a3-e2ff484be372",
      "name": "Smart Text",
      "description":
        "Use SMS to help you communicate with your customers.",
      "categories": ["Channels"],
      "subscriptions": [
        {
          "name": "Trial",
          "price": 0
        }
      ]
    },
    {
      "id": "8d68c357-59e6-505a-b0e1-4953196b14df",
      "name": "Customer Chat",
      "description": "Improve your call center with live chat support.",
      "categories": ["Channels"],
      "subscriptions": [
        {
          "name": "Trial",
          "price": 0
        }
      ]
    }]);

    expect(menuCategories).toStrictEqual([{"value": "Channels"}]);
  });

  it('Should format currency', () => {
    const formatted = formatCurrency([{
      "id": "52714d80-e3c4-5593-b9a3-e2ff484be372",
      "name": "Smart Text",
      "description":
        "Use SMS to help you communicate with your customers.",
      "categories": ["Channels"],
      "subscriptions": [
        {
          "name": "Starter",
          "price": 2000
        },
        {
          "name": "Plus",
          "price": 4500
        }
      ]
    },
    {
      "id": "dd024ed5-efae-5785-addc-09e592066e5c",
      "name": "Report Plus",
      "description": "Advanced reporting with custom dashboards.",
      "categories": ["Reporting"],
      "subscriptions": [
        {
          "name": "Trial",
          "price": 0
        }
      ]
    }]);
    expect(formatted).toStrictEqual([{
      "id": "52714d80-e3c4-5593-b9a3-e2ff484be372",
      "name": "Smart Text",
      "description":
        "Use SMS to help you communicate with your customers.",
      "categories": ["Channels"],
      "subscriptions": [
        {
          "name": "Starter",
          "price": "20.00"
        },
        {
          "name": "Plus",
          "price": "45.00"
        }
      ]
    },
    {
      "id": "dd024ed5-efae-5785-addc-09e592066e5c",
      "name": "Report Plus",
      "description": "Advanced reporting with custom dashboards.",
      "categories": ["Reporting"],
      "subscriptions": [
        {
          "name": "Trial",
          "price": "0"
        }
      ]
    }]);
  })

});