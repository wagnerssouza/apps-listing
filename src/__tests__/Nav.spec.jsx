import React from "react";
import { fireEvent, render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import Nav from "../components/Nav";
import { useMenu } from "../hooks/useMenu";
import { useListAppsFilteredByCategory } from "../hooks/useListAppsFilteredByCategory";

const mockSpy = jest.fn();
jest.mock("../hooks/useMenu");
jest.mock("../hooks/useListAppsFilteredByCategory");

describe("Component Nav", () => {

  it("Should render Nav component", () => {
    useMenu.mockImplementation(() => {
      return {
        menuCategories: [{ value: "Channels" }],
      };
    });

    useListAppsFilteredByCategory.mockImplementation(() => {
      return {
        updateListAppsFilteredByCategory: mockSpy,
      };
    });

    const { getByText } = render(<Nav />);
    const listItem = getByText("Categories");
    
    expect(listItem).toBeInTheDocument();
  });

  it("Should render navigation menu", () => {
    useMenu.mockImplementation(() => {
      return {
        menuCategories: [{ value: "Channels" }],
      };
    });

    useListAppsFilteredByCategory.mockImplementation(() => {
      return {
        updateListAppsFilteredByCategory: mockSpy,
      };
    });

    const { getByLabelText } = render(<Nav />);

    const listItems = getByLabelText("nav-menu");
    expect(listItems).toMatchInlineSnapshot(`
      <ul
        aria-label="nav-menu"
        class="nav-menu"
      >
        <li>
          <a
            class="active"
            href="#"
            id="All"
          >
            All
          </a>
        </li>
        <li>
          <a
            class=""
            href="#"
            id="Channels"
          >
            Channels
          </a>
        </li>
      </ul>
    `);
  });

  it("Should check active class in list item clicked", () => {
    
    const { getByText } = render(<Nav />);
    useMenu.mockImplementation(() => {
      return {
        menuCategories: [{ value: "Channels" }],
      };
    });

    useListAppsFilteredByCategory.mockImplementation(() => {
      return {
        updateListAppsFilteredByCategory: mockSpy,
      };
    });

    const listItem = getByText("Channels");
    fireEvent.click(listItem);
    
    expect(listItem).toHaveClass("active");
  });

});