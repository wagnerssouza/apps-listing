# Project: apps_listing v0.1

<img src="https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white" />
<img src="https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white" />
<img src="https://img.shields.io/badge/Javascript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E" />
<img src="https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white" />
<img src="https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB" />
<br />

[![site](https://snipboard.io/eRjFyJ.jpg)]()

## Features previstas

Abaixo os itens contemplados nessa entrega:

- [x] Liste todos os aplicativos
- [x] Paginar a lista (tamanho da página = 3)
- [x] Filtre os aplicativos conforme você digita na barra de pesquisa
- [x] Ter todas as categorias existentes na navegação à esquerda classificadas por ordem alfabética
- [x] Permitir a filtragem de aplicativos quando clicamos em uma categoria
- [x] Os aplicativos devem ser classificados em ordem crescente da soma dos preços dos planos

## 💻 Liste todos os aplicativos
A lista de aplicativos é exibida por completo. <br />
Temos 10 itens divididos em 4 páginas, 3 páginas com 3 itens e 1 página com 1 item.<br /><br />

## 💻 Paginar a lista (tamanho da página = 3)
A exibição dos itens é limitada em no máximo 3 e no mínimo 1 por página<br /><br />

## 💻 Filtre os aplicativos conforme você digita na barra de pesquisa
Existe um campo de formulário que filtra os aplicativos que estão na paginação, a busca é livre, ou seja, por qualquer termo.<br />
Os itens retornados são paginados mantendo a regra de no máximo 3 e no mínimo 1 por página<br />
Caso seu filtro não retorne um resultado, uma mensagem é exibida ao usuário.<br /><br />

## 💻 Ter todas as categorias existentes na navegação à esquerda classificadas por ordem alfabética
O menu de categorias do lado esquerdo é montado dinamicamente a partir da lista de aplicativos e ordenado em ordem alfabética<br /><br />


## 💻 Permitir a filtragem de aplicativos quando clicamos em uma categoria
Ao clicar em alguma das categorias do menu lateral esquerdo, a lista/paginação é atualizada somente com itens da categoria clicada.<br />
A funcionalidade de filtrar busca somente nos itens da categoria selecionada.

## 💻 Os aplicativos devem ser classificados em ordem crescente da soma dos preços dos planos
A lista de aplicativos é ordenada de forma crescente, ou seja, do menor para o maior.

## 💻 Pré-requisitos

Para executar o sistema, siga os passos abaixo:

* Faça um clone do repositório: `git clone https://gitlab.com/wagnerssouza/apps-listing.git`
* Acesse o diretório: `cd apps-listing/`
* Instalando pacotes: `npm install`
* Executando projeto em ambiente local: `npm run dev` - `http://localhost:3000`

## 🚀 Executando testes

Para executar os testes siga as instruções abaixo:

`npm run test` ou `npm run coverage`

[![site](https://snipboard.io/D0zPku.jpg)]()

## 🤝 Colaborador

Agradeço imensamente a oportunidade em participar desse processo seletivo. Muito obrigado :heart:

<table>
  <tr>
    <td align="center">
      <a href="https://www.linkedin.com/in/wagnerssouza90/">
        <img src="https://avatars.githubusercontent.com/u/9846866?v=4" width="100px;" alt="Wagner Souza"/><br>
        <sub>
          <b>Wagner Souza</b>
        </sub>
      </a>
    </td>    
  </tr>
</table>
